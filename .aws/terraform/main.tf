data "aws_caller_identity" "current" {}

module "namespace" {
  source              = "module-registry.services.ally.com/ally/name-and-tags/aws"
  version             = "~> 1.0"
  application_name    = var.application_name
  service_name        = var.service_name
  workspace           = terraform.workspace
  environment         = var.environment
  application_id      = var.application_id
  data_classification = var.data_classification
  issrcl_level        = var.issrcl_level
  owner               = var.owner
  scm_project         = var.scm_project
  scm_repo            = var.scm_repo
  scm_branch          = var.scm_branch
  scm_commit_id       = var.scm_commit_id
  additional_tags = {
    "tf_starter" = var.creation_date
  }
}

# Documentation: https://cre.ally.corp/docs/modules/state/v2/readme.html
module "state" {
  source        = "module-registry.services.ally.com/ally/state/aws"
  version       = "~> 2.0"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
}

# Documentation: https://cre.ally.corp/docs/modules/lambda/v7/readme.html
module "lambda" {
  source        = "module-registry.services.ally.com/ally/lambda/aws"
  version       = "~> 7.0"
  namespace     = module.namespace.lower_short_name
  name          = "main"
  code          = "../../src/"
  handler       = "main.lambda_handler"
  timeout       = 3
  size          = 128
  runtime       = "python3.8"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags
}

