variable "region" {
  type        = string
  description = "The target AWS region to deploy to."
  default     = "us-east-1"
}

variable "environment" {
  type        = string
  description = "Env to use for resolving variables"
  default     = "default"
}

variable "application_id" {
  type        = string
  description = "The Ally Application ID of the project."
}

variable "application_name" {
  type        = string
  description = "The Ally Application Name of the project."
}

variable "service_name" {
  type        = string
  description = "The Ally Application service name."
}

variable "data_classification" {
  type        = string
  description = "The Ally data classification."
}

variable "issrcl_level" {
  type        = string
  description = "The Information System Security Risk Classification Level for the resource. This value should come from the ServiceNow application catalog"
}

variable "owner" {
  type        = string
  description = "Team owner of the application"
}

variable "scm_project" {
  type        = string
  description = "The Bitbucket project used to deploy the resource."
}

variable "scm_repo" {
  type        = string
  description = "The Bitbucket repository used to deploy the release."
}

variable "scm_branch" {
  type        = string
  description = "The Bitbucket and tag used to deploy the resource."
  default     = "local-deploy"
}

variable "scm_commit_id" {
  type        = string
  description = "The specific commit that was used to deploy the resource."
  default     = "000000"
}

variable "creation_date" {
  type        = string
  description = "The date and time this project was generated."
  default     = "2023-02-10T20:59:01.355558"
}

variable "awsacct_cidr_code" {
  type        = map
  description = "The CIDR block assigned to the account VPC"
}

