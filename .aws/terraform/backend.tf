terraform {
  backend "s3" {
    bucket         = "ally-us-east-1-551570330574-tf-states"
    key            = "104964-auto-followup-xzql4f/terraform.tfstate"
    dynamodb_table = "ally-us-east-1-551570330574-tf-locks"
    region         = "us-east-1"
    encrypt        = true
  }
}

