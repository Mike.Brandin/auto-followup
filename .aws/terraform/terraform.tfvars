# Name & Tagging
application_id      = "104964"
application_name    = "xzql4f"
service_name        = "auto-followup"
owner               = "xzql4f"
data_classification = "Proprietary"
issrcl_level        = "Low"
scm_project         = "Mike.Brandin"
scm_repo            = "auto-followup"

awsacct_cidr_code = {
  default = "010-075-060-000"
  dev     = "010-075-060-000"
  qa      = "<< Enter CIDR >>"
  cap     = "<< Enter CIDR >>"
  psp     = "<< Enter CIDR >>"
  prod    = "<< Enter CIDR >>"
}

